package clases;

public class Arma {
	private String codigoSerie;
	private double valor;
	private double peso;
	private String calibre;
	private double alcance;

	/**
	 * Constructor que recibe todos los atributos como parametros.
	 * 
	 * @param codigo  codigo
	 * @param valor   valor
	 * @param peso    peso
	 * @param calibre calibre
	 * @param alcance alcance
	 */
	public Arma(String codigo, double valor, double peso, String calibre, double alcance) {
		this.codigoSerie = codigo;
		this.valor = valor;
		this.peso = peso;
		this.calibre = calibre;
		this.alcance = alcance;
	}

	/**
	 * Metodo devuelve codigo de serie.
	 *
	 * @return codigo serie
	 */
	public String getCodigoSerie() {
		return codigoSerie;
	}

	/**
	 * Cambia codigo serie.
	 *
	 * @param codigoSerie the new codigo serie
	 */
	public void setCodigoSerie(String codigoSerie) {
		this.codigoSerie = codigoSerie;
	}

	/**
	 * Devuelve valor.
	 *
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * Cambia valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

	/**
	 * Devuelve peso.
	 *
	 * @return peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * Cambia peso.
	 *
	 * @param peso the new peso
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}

	/**
	 * Devuelve calibre.
	 *
	 * @return calibre
	 */
	public String getCalibre() {
		return calibre;
	}

	/**
	 * Cambia calibre.
	 *
	 * @param calibre the new calibre
	 */
	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}

	/**
	 * Devuelve alcance.
	 *
	 * @return alcance
	 */
	public double getAlcance() {
		return alcance;
	}

	/**
	 * Cambia alcance.
	 *
	 * @param alcance the new alcance
	 */
	public void setAlcance(double alcance) {
		this.alcance = alcance;
	}

	@Override
	public String toString() {
		return "Arma [codigoSerie=" + codigoSerie + ", valor=" + valor + ", peso=" + peso + ", calibre=" + calibre
				+ ", alcance= " + alcance + "]";
	}

}
