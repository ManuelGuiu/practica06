package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorPropietarios {
	
	private ArrayList<Arma> listaArmas;
	private ArrayList<Propietario> listaPropietarios;

	public GestorPropietarios() {
		listaArmas = new ArrayList<Arma>();
		listaPropietarios = new ArrayList<Propietario>();
	}

	/**
	 * Da de alta un arma.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 * @param peso the peso
	 * @param calibre the calibre
	 * @param alcance the alcance
	 */
	public void altaArma(String codigo, double valor, double peso, String calibre, double alcance) {
		if (!existeArma(codigo)) {
			Arma nuevaArma = new Arma(codigo, valor, peso, calibre, alcance);
			listaArmas.add(nuevaArma);
		} else {
			System.out.println("El arma ya existe.");
		}
	}

	/**
	 * Elimina un arma arma.
	 *
	 * @param codigoSerie the codigo serie
	 */
	public void eliminarArma(String codigoSerie) {
		Iterator<Arma> iteradorArma = listaArmas.iterator();
		if(existeArma(codigoSerie)) {
			while (iteradorArma.hasNext()) {
				Arma arma = iteradorArma.next();
				if (arma.getCodigoSerie().equals(codigoSerie)) {
					iteradorArma.remove();
				}
			}
		}else {
			System.out.println("No existe.");
		}
		
	}

	/**
	 * Comprueba que existe el arma.
	 *
	 * @param codigo the codigo
	 * @return true, if successful
	 */
	public boolean existeArma(String codigo) {
		for (Arma arma : listaArmas) {
			if (arma != null && arma.getCodigoSerie().equals(codigo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Lista todas las armas.
	 */
	public void listarArmas() {
		for (Arma arma : listaArmas) {
			if (arma != null) {
				System.out.println(arma);
			}
		}
	}

	/**
	 * Busca armas por su codigo.
	 *
	 * @param codigo the codigo
	 * @return the arma
	 */
	public Arma buscarArma(String codigo) {
		for (Arma arma : listaArmas) {
			if (arma != null && arma.getCodigoSerie().equals(codigo)) {
				return arma;
			}
		}
		return null;
	}

	/**
	 * Da de alta un propietario.
	 *
	 * @param nombre the nombre
	 * @param licencia the licencia
	 * @param edad the edad
	 */
	public void altaPropietario(String nombre, String licencia, int edad) {
		if (!existePropietario(licencia)) {
			Propietario nuevo = new Propietario(nombre, licencia, edad);
			listaPropietarios.add(nuevo);
		}
	}

	/**
	 * Comprueba existe propietario.
	 *
	 * @param licencia the licencia
	 * @return true, if successful
	 */
	public boolean existePropietario(String licencia) {
		for (Propietario propietario : listaPropietarios) {
			if (propietario != null && propietario.getLicencia().equals(licencia)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Busca propietario por su licencia.
	 *
	 * @param licencia the licencia
	 * @return the propietario
	 */
	public Propietario buscarPropietario(String licencia) {
		for (Propietario propietario : listaPropietarios) {
			if (propietario != null && propietario.getLicencia().equals(licencia)) {
				return propietario;
			}
		}
		return null;
	}

	/**
	 * Eliminar propietario por su licencia.
	 *
	 * @param licencia the licencia
	 */
	public void eliminarPropietario(String licencia) {
		if(existePropietario(licencia)) {
			Iterator<Propietario> iteradorPropietario = listaPropietarios.iterator();

			while (iteradorPropietario.hasNext()) {
				Propietario pro = iteradorPropietario.next();
				if (pro.getLicencia().equals(licencia)) {
					iteradorPropietario.remove();
				}
			}
		}else {
			System.out.println("No existe.");
		}
		
	}

	/**
	 * Asigna un arma a un propietario.
	 *
	 * @param codigoSerie the codigo serie
	 * @param licencia the licencia
	 */
	public void asignarArma(String codigoSerie, String licencia) {
		if (buscarArma(codigoSerie) != null && buscarPropietario(licencia) != null) {
			Arma arma = buscarArma(codigoSerie);
			Propietario propietario = buscarPropietario(licencia);
			propietario.setArma(arma);
		}
	}

	/**
	 * Listar todos los propietarios por edad.
	 *
	 * @param edad the edad
	 */
	public void listarPropietarios(int edad) {
		for (Propietario propietario : listaPropietarios) {
			if (propietario != null && propietario.getEdad() == edad) {
				System.out.println(propietario);
			}
		}
	}
	
	/**
	 * Listar todos los propietarios.
	 */
	public void listarPropietarios() {
		for (Propietario propietario : listaPropietarios) {
			if (propietario != null) {
				System.out.println(propietario);
			}
		}
	}

	/**
	 * Listar propietario de un arma.
	 *
	 * @param codigoSerie the codigo serie
	 */
	public void listarPropietarioDeArma(String codigoSerie) {
		for (Propietario propietario : listaPropietarios) {
			if (propietario.getArma() != null && propietario.getArma().getCodigoSerie().equals(codigoSerie)) {
				System.out.println(propietario);
			}
		}
	}
}
