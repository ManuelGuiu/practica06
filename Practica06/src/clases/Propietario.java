package clases;

public class Propietario {
	
	private String nombre;
	private String licencia;
	private int edad;
	private Arma arma;

	/**
	 * Constructor con parametros.
	 *
	 * @param nombre the nombre
	 * @param licencia the licencia
	 * @param edad the edad
	 */
	public Propietario(String nombre, String licencia, int edad) {
		this.nombre = nombre;
		this.licencia = licencia;
		this.edad = edad;
	}

	/**
	 * Gets nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets licencia.
	 *
	 * @return the licencia
	 */
	public String getLicencia() {
		return licencia;
	}

	/**
	 * Sets licencia.
	 *
	 * @param licencia the new licencia
	 */
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	/**
	 * Gets edad.
	 *
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * Sets edad.
	 *
	 * @param edad the new edad
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

	/**
	 * Gets arma.
	 *
	 * @return the arma
	 */
	public Arma getArma() {
		return arma;
	}

	/**
	 * Sets arma.
	 *
	 * @param arma the new arma
	 */
	public void setArma(Arma arma) {
		this.arma = arma;
	}
	
	@Override
	public String toString() {
		return "Propietario [nombre= " + nombre + ", licencia= " + licencia + ", edad= " + edad + ", arma= " + arma
				+ "]";
	}

}