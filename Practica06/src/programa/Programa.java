package programa;

import java.util.Scanner;

import clases.GestorPropietarios;

public class Programa {
	
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		GestorPropietarios gestor = new GestorPropietarios();
		int numero = 1;
		String cadena = "";
		while (numero < 4) {
			System.out.println("Da de alta el arma numero " + numero);
			System.out.println("Introduce su codigo:");
			String codigo = input.nextLine();
			System.out.println("Introduce su valor.");
			double valor = input.nextDouble();
			System.out.println("Introduce su peso.");
			double peso = input.nextDouble();
			input.nextLine();
			System.out.println("Introduce su calibre.");
			String calibre = input.nextLine();
			System.out.println("Introduce su alcance.");
			double alcance = input.nextDouble();
			input.nextLine();
			gestor.altaArma(codigo, valor, peso, calibre, alcance);
			numero++;
		}
		System.out.println("Has dado de alta las siguientes armas");
		gestor.listarArmas();

		boolean continuar = true;
		while (continuar) {
			System.out.println("Introduce la opcion que deseas realizar:");
			System.out.println("1. Buscar un arma por su codigo de serie.");
			System.out.println("2. Eliminar un arma de las introducidas.");
			numero = comprobarEntero();
			switch (numero) {
			case 1:
				System.out.println("Introduce su codigo.");
				cadena = input.nextLine();
				if (gestor.buscarArma(cadena) == null) {
					System.out.println("No existe.");
				} else {
					System.out.println(gestor.buscarArma(cadena));
				}
				break;
			case 2:
				System.out.println("Introduce su codigo.");
				cadena = input.nextLine();
				gestor.eliminarArma(cadena);
				break;
			default:
				System.out.println("Introduce una opcion valida:");
				continuar = true;
			}
			System.out.println("�Deseas realizar alguna operacion de nuevo? s/n");
			cadena = input.nextLine();
			if (cadena.equalsIgnoreCase("s") || cadena.equalsIgnoreCase("si") || cadena.equalsIgnoreCase("y")
					|| cadena.equalsIgnoreCase("yes")) {
				continuar = true;
			} else if (cadena.equalsIgnoreCase("n") || cadena.equalsIgnoreCase("no")) {
				continuar = false;
			} else {
				System.out.println("Respuesta no reconocida, ignorando usuario.");
				continuar = false;
			}
		}
		numero = 1;
		while (numero < 4) {
			System.out.println("Introduce el propietario numero " + numero);
			System.out.println("Introduce el nombre.");
			String nombre = input.nextLine();
			System.out.println("Introduce su licencia.");
			String licencia = input.nextLine();
			System.out.println("Introduce su edad");
			int edad = comprobarEntero();
			gestor.altaPropietario(nombre, licencia, edad);
			numero++;
		}
		System.out.println("Has dado de alta los siguientes propietarios.");
		gestor.listarPropietarios();

		continuar = true;
		while (continuar) {
			System.out.println("1. Buscar propietario");
			System.out.println("2. Eliminar propietario.");
			System.out.println("3. Listar propietarios por edad.");
			System.out.println("4. Listar propietario por su arma.");
			System.out.println("5. Registrar arma a un propietario.");
			numero = comprobarEntero();
			switch (numero) {
			case 1:
				System.out.println("Introduce la licencia.");
				cadena = input.nextLine();
				if(gestor.buscarPropietario(cadena) == null) {
					System.out.println("No existe.");
				}else {
					System.out.println(gestor.buscarPropietario(cadena));
				}
				break;
			case 2:
				System.out.println("Introduce la licencia.");
				cadena = input.nextLine();
				gestor.eliminarPropietario(cadena);
				break;
			case 3:
				System.out.println("Introduce la edad.");
				numero = comprobarEntero();
				gestor.listarPropietarios(numero);
				break;
			case 4:
				System.out.println("Introduce su codigo de serie.");
				cadena = input.nextLine();
				gestor.listarPropietarioDeArma(cadena);
				break;
			case 5:
				System.out.println("Introduce el codigo de serie.");
				String codigoSerie = input.nextLine();
				System.out.println("Introduce la licencia.");
				String licencia = input.nextLine();
				gestor.asignarArma(codigoSerie, licencia);
				break;
			default:

			}
			System.out.println("�Deseas realizar alguna operacion de nuevo? s/n");
			cadena = input.nextLine();
			if (cadena.equalsIgnoreCase("s") || cadena.equalsIgnoreCase("si") || cadena.equalsIgnoreCase("y")
					|| cadena.equalsIgnoreCase("yes")) {
				continuar = true;
			} else if (cadena.equalsIgnoreCase("n") || cadena.equalsIgnoreCase("no")) {
				continuar = false;
			} else {
				System.out.println("Respuesta no reconocida, ignorando usuario.");
				continuar = false;
			}
		}
		
	}

	/**
	 * Comprobar entero te pide un numero hasta que sea entero y lo devuelve.
	 *
	 * @return the int
	 */
	public static int comprobarEntero() {
		boolean continuar = true;
		int numero = 0;
		String entrada = "";
		while (continuar) {
			continuar = false;
			entrada = input.nextLine();
			for (int i = 0; i < entrada.length(); i++) {
				if (entrada.charAt(i) <= '0' || entrada.charAt(i) >= '9') {
					continuar = true;
					System.out.println("Introduce un valor valido.");
					break;
				}
			}
		}
		numero = Integer.parseInt(entrada);
		return numero;
	}

}
